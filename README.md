# Flappy Kitty #
## What is this sh..? ##
Flappy Kitty is a Flappy Bird-like engine for GameMaker: Studio, made in GameMaker: Studio Professional.
With Flappy Kitty, you can simply start to develop your own Flappy Bird-like game in GameMaker: Studio without the need to do it from scratch.

## How do I start? ##
You need to change the sprites that'll come with the project (because, you know, they're mine and your finished game should be build on your own ressources. That said, you're offically **not** allowed to publish your game with the default graphics. That's all.)

You can add new obstacles by adding new sprites and objects to the folders in Sprites/Obstacles and Objects/Obstacles. Obstacle-Objects need to have "Obstacles_Group" set as the parent object.

Now you can add them to the arrays in Scripts/Level_1_Obstacles and you're done with the basic steps.

The moving backgrounds are just normal backgrounds set in the room properties.

## Copyright ##
Author: [Alice Peters](https://www.alicepeters.de/)

*All rights on the graphics and content are based on the german copyright which means that you need the permission of the original author if you want to use them in any other ways than described.*
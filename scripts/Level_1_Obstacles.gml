Script_Handler.obstacle_step++;
random_position = irandom_range(view_hview[0]/2-100, view_hview[0]/2+100);

obstacle_top[0] = obj_Lamp;

obstacle_bottom[0] = obj_Boxes;

if(Script_Handler.obstacle_step == 200) {
    Script_Handler.obstacle_step = 0;
    num_top = random(array_length_1d(obstacle_top)-1);
    num_bottom = random(array_length_1d(obstacle_bottom)-1);
    instance_create(view_wview[0]+50, random_position-37, obstacle_top[num_top]);
    instance_create(view_wview[0]+50, random_position+37, obstacle_bottom[num_bottom]);
    instance_create(view_wview[0]+50, 0, obj_Scorecheck);
}
